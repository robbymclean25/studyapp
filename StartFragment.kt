package com.example.studyapp.view.fragments
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.os.Bundle
import com.example.studyapp.R

import androidx.navigation.fragment.findNavController
import com.example.studyapp.databinding.FragmentStartBinding

class StartFragment: Fragment(R.layout.fragment_start) {

    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) : View? {FragmentStartBinding.inflate(inflater, container, false)

        return binding.root
    }

 fun initViews(){
     binding.btnStart.setOnClickListener {
        val action = StartFragmentDirections.actionStartFragmentToFlashCardFragment()
        findNavController().navigate(action)

    }
}


}