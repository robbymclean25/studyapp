package com.example.studyapp.view.fragments
import com.example.studyapp.R
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.studyapp.databinding.FragmentFlashCardBinding

class FlashCardFragment : Fragment(R.layout.fragment_flash_card) {

    /**
     * To access the widgets use the code below
     *
     * Topic TextView -> binding.tvTopic
     * Close Button -> binding.ivClose
     * Previous Button -> binding.btnPrev
     * Next Button -> binding.btnNext
     *
     * To enable and disable a view use example below
     * binding.btnNext.isEnabled
     */
    private var _binding: FragmentFlashCardBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFlashCardBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}